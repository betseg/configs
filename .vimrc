set nocompatible
filetype plugin indent on

set mouse=a
set number
set autoindent
set smartindent
set hlsearch
set ruler
set laststatus=2
set wrap
set whichwrap+=<,>,h,l,[,]
set conceallevel=2
set concealcursor=nc
set termguicolors
set splitbelow
set splitright
let &t_ut=''
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

syntax on
set cursorline
let base16colorspace=256
"packadd! dracula
"colorscheme dracula
colorscheme base16-dracula
let g:airline_theme="dracula"
"colorscheme fairyfloss
"let g:airline_theme="fairyfloss"

set ts=4 sw=4 et cc=80 t_Co=256
set list
set listchars=tab:▸\ ,eol:¬,extends:,precedes:,trail:·

noremap <Leader>y "+y
noremap <Leader>p "+p
noremap <Leader>Y "+Y
noremap <Leader>P "+P

let g:user_emmet_install_global = 0
autocmd FileType html,css EmmetInstall

let g:airline#extensions#ale#enabled = 1
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#branch#empty_message = ''
let g:airline#extensions#branch#format = 1
let g:airline#extensions#tabline#enabled = 1

let g:airline_detect_modified=1
let g:airline_detect_paste=1

"let g:airline_powerline_fonts = 1

let g:ycm_autoclose_preview_window_after_completion = 1
"let g:ycm_global_ycm_extra_conf = '/home/betseg/.vim/pack/misc/start/YouCompleteMe/.ycm_extra_conf.py'
let g:ycm_language_server =
\ [
\   {
\     'name': 'rust',
\     'cmdline': ['rust-analyzer'],
\     'filetypes': ['rust'],
\     'project_root_files': ['Cargo.toml']
\   }
\ ]

let g:tmuxline_preset = {
      \'b'    : '#S',
      \'c'    : '#W',
      \'win'  : '#I #W',
      \'cwin' : '#I #W',
      \'x'    : ['%R', '%a', '%Y-%m-%d'],
      \'y'    : ['#(cat /sys/class/power_supply/BAT0/status)', '#(cat /sys/class/power_supply/BAT0/capacity)%%'] }

"dab my extension
"let g:dcrpc_autostart = 1

packloadall
silent! helptags ALL
